import React from 'react';
import { StyleSheet, View, Button, Linking } from 'react-native';
//Importing Routing components Router, Switch, Route...
import { Router, Switch, Route } from './routing'; 
//Importing "export default Home" from Home.js
import Home from './Home';
import Pokemon from './Pokemon';

export default class App extends React.Component {
  state = {
    selectedPokemon: null
  };
  selectPokemon = selectedPokemon => {
    this.setState({
      selectedPokemon
    });
  };
  _openBrowser(){
    Linking.openURL("https://google.com/")};
  _openMailTo(){
      Linking.openURL("mailto: zachary.fasenmyer@innove.com");
    };
  _openSMS(){
      Linking.openURL("sms: 2108706061")};
  render() {
    console.log("App.render()");
    //Rendering return
    return (
      console.log("App.return()"),//Use a comma to indicate the presence of more events
    //Within Render, Return uses Router/Switch/Route to: 
      <View style={styles.container}>
        <Router>
          <Switch>
            <Route
              //Ensures an exact copy of input information
              exact 
              //Establishing a path for Navigation... Resets upon connection
              path="/" 
              //Initiates Home screen after path is reset
              render={props => ( 
              //Opening function from Home.js, 
              //Passing all {props} (arguments)
              //selectPokemon: null (reset/re.state.ed to zero) equals= 
              //this (src/App.js's) selectPokemon function
                <Home {...props} selectPokemon={this.selectPokemon} />
              )}
            />
            <Route
              path="/pokemon"
              render={props => (
                <Pokemon
                  selectedPokemon={this.state.selectedPokemon}
                  {...props}
                />
              )}
            />
          </Switch>
        </Router>
        <View style={styles.buttonContainer}>
        <Button
          onPress={this._openBrowser}
          title="Open browser"
          color="red"
        />
        <Button
          onPress={this._openMailTo}
          title="Open Mail to"
          color="blue"
        />
        <Button
          onPress={this._openSMS}
          title="Open SMS"
          color="green"
        />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50,
    padding: 50
  }
});
